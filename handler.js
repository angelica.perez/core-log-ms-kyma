
const { MongoClient } = require('mongodb');
const { v4: uuidv4 } = require('uuid');

// MongoDB connection URL and database name
const mongoUrl = process.env.MONGO_URI || 'mongodb://localhost:27017/timp-dev';
const dbName = process.env.DB_NAME || 'logsDB';

let logsCollection;

(async () => {
    const client = new MongoClient(mongoUrl);
    await client.connect();
    const db = client.db(dbName);
    logsCollection = db.collection('logs');
})();

async function get(req, res) {
    try {
        const logs = await logsCollection.find().sort({ timestamp: -1 }).toArray();
        res.status(200).send(logs);
    } catch (error) {
        console.error('Error fetching logs from MongoDB', error);
        res.status(500).send('Failed to fetch logs from MongoDB');
    }
}

async function post(req, res) {
    try {
        const logData = {
            id: uuidv4(),
            message: req.body.message || 'Hello logs!',
            timestamp: new Date(),
        };

        await logsCollection.insertOne(logData);

        res.send('Log saved to MongoDB!');
    } catch (error) {
        console.error('Error writing to MongoDB', error);
        res.status(500).send('Failed to write log to MongoDB');
    }
}

async function handleEvent(event) {
    try {
        const logData = event.data;
        logData.timestamp = new Date();

        await logsCollection.insertOne(logData);
        console.info('Received log from Kyma event and saved to MongoDB');
    } catch (error) {
        console.error('Error writing event log to MongoDB', error);
        throw new Error('Failed to write log to MongoDB');
    }
}

async function handleRestRequest(request, response) {
    switch (request.method) {
        case "GET":
            return get(request, response);
        case "POST":
            return post(request, response);
        default:
            response.status(400).send("Bad request");
    }
}

module.exports = {
    main: async function (event, context) {
        console.log(event)
        let request = event.extensions.request;
        let response = event.extensions.response;
        await handleRestRequest(request, response);
        return;

    }
};
